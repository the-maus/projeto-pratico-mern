import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import $ from 'jquery';
import Popper from 'popper.js';
import "bootstrap/dist/js/bootstrap.bundle.min.js";
import "bootstrap/dist/css/bootstrap.min.css";

import Navbar from "./components/navbar.component";
import AppFooter from "./components/footer.component";

import SongsList from "./components/songs/list.component";
import CreateSong from "./components/songs/create.component";
import EditSong from "./components/songs/edit.component";

import ArtistsList from "./components/artists/list.component";
import CreateArtist from "./components/artists/create.component";
import EditArtist from "./components/artists/edit.component";

import GenresList from "./components/genres/list.component";
import CreateGenre from "./components/genres/create.component";
import EditGenre from "./components/genres/edit.component";

import RecordLabelList from "./components/record_labels/list.component";
import CreateLabel from "./components/record_labels/create.component";
import EditLabel from "./components/record_labels/edit.component";

import AlbumsList from "./components/albums/list.component";
import CreateAlbum from "./components/albums/create.component";
import EditAlbum from "./components/albums/edit.component";

import ArtistSongsList from "./components/songs/artist-song.component";
import GenreSongsList from "./components/songs/genre-song.component";
import LabelAlbumsList from "./components/albums/label-albums.component";

function App() {
  return (
    <Router>
      <div className="container">
        <AppFooter />
        <Navbar />
        <br/>
        <Route path="/songs" exact component={SongsList} />
        <Route path="/create-song" component={CreateSong} />
        <Route path="/edit-song/:id" component={EditSong} />

        <Route path="/artists" exact component={ArtistsList} />
        <Route path="/create-artist" component={CreateArtist} />
        <Route path="/edit-artist/:id" component={EditArtist} />

        <Route path="/genres" exact component={GenresList} />
        <Route path="/create-genre" component={CreateGenre} />
        <Route path="/edit-genre/:id" component={EditGenre} />

        <Route path="/labels" exact component={RecordLabelList} />
        <Route path="/create-label" component={CreateLabel} />
        <Route path="/edit-label/:id" component={EditLabel} />

        <Route path="/albums" exact component={AlbumsList} />
        <Route path="/create-album" component={CreateAlbum} />
        <Route path="/edit-album/:id" component={EditAlbum} />

        <Route path="/songs-by-artist" component={ArtistSongsList} />
        <Route path="/songs-by-genre" component={GenreSongsList} />
        <Route path="/albums-by-label" component={LabelAlbumsList} />
      </div>
    </Router>
  );
}

export default App;
