import React, { Component } from 'react';
import axios from 'axios';

export default class EditLabel extends Component {
    constructor(props) {
        super(props);

        this.onChangeName = this.onChangeName.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            name: '',
        }
    }

    componentDidMount() {
        axios.get('http://localhost:5000/labels/'+this.props.match.params.id)
            .then(res => {
                this.setState({
                    name: res.data.name
                })
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    onChangeName(e) {
        this.setState({
            name: e.target.value
        });
    }

    onSubmit(e) {
        e.preventDefault();

        const label = {
            name: this.state.name,
        }

        console.log(label);

        axios.post('http://localhost:5000/labels/update/'+this.props.match.params.id, label)
            .then(res => console.log(res.data));

        window.location = '/labels';
    }

    render() {
        return (
            <div>
                <h3>Editar gravadora</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Nome: </label>
                        <input type="text" required className="form-control" value={this.state.name} onChange={this.onChangeName}/>
                    </div>
                    <div className="form-group">
                        <input type="submit" required className="btn btn-primary" value="Enviar"/>
                    </div>
                </form>
            </div>
        );
    }
}