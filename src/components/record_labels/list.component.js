import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

const RecordLabel = props => (
    <tr>
        <td>{props.label.name}</td>
        <td>
            <Link to={"/edit-label/"+props.label._id}>editar</Link> | <a href="#" onClick={() => props.deleteLabel(props.label._id)}>remover</a>
        </td>
    </tr>
)

export default class RecordLabelList extends Component {
    constructor(props) {
        super(props);

        this.deleteLabel = this.deleteLabel.bind(this);

        this.state = {labels:[]};
    }

    componentDidMount() {
        axios.get('http://localhost:5000/labels/')
            .then(res => {
                this.setState({labels: res.data});
            })
            .catch((error) => {
                console.log(error);
            })
    }

    deleteLabel(id) {
        axios.delete('http://localhost:5000/labels/' + id)
            .then(res => console.log(res.data));

        this.setState({
            labels: this.state.labels.filter(el => el._id !== id)
        })
    }

    labelList() {
        return this.state.labels.map(currentLabel => {
            return <RecordLabel label={currentLabel} deleteLabel={this.deleteLabel} key={currentLabel._id}/>
        })
    }

    render() {
        return (
          <div>
              <h3>Gravadoras</h3>
              <table className="table">
                  <thead className="thead-light">
                      <tr>
                          <th>Nome</th>
                          <th>Ações</th>
                      </tr>
                  </thead>
                  <tbody>
                      { this.labelList() }
                  </tbody>
              </table>
          </div>  
        );
    }
}