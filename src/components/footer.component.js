import React, { Component } from 'react';

export default class AppFooter extends Component {
    
    render () {
        return (
            <div>
                <div className="container">
                    <h6>Sistema para controle de informações sobre obras musicais</h6>
                    <p style={{fontSize: "12px"}}>Desenvolvido para a disciplina de Programação Web 2 da Pós-graduação em Desenvolvimento Web do IFBA - Campus Vitória da Conquista</p>
                    <p style={{fontSize: "12px"}}>Autor: Matheus Sampaio</p>
                </div>
            </div>
        )
    }
}