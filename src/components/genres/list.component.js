import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

const Genre = props => (
    <tr>
        <td>{props.genre.name}</td>
        <td>
            <Link to={"/edit-genre/"+props.genre._id}>editar</Link> | <a href="#" onClick={() => props.deleteGenre(props.genre._id)}>remover</a>
        </td>
    </tr>
)

export default class GenresList extends Component {
    constructor(props) {
        super(props);

        this.deleteGenre = this.deleteGenre.bind(this);

        this.state = {genres:[]};
    }

    componentDidMount() {
        axios.get('http://localhost:5000/genres/')
            .then(res => {
                this.setState({genres: res.data});
            })
            .catch((error) => {
                console.log(error);
            })
    }

    deleteGenre(id) {
        axios.delete('http://localhost:5000/genres/' + id)
            .then(res => console.log(res.data));

        this.setState({
            genres: this.state.genres.filter(el => el._id !== id)
        })
    }

    genreList() {
        return this.state.genres.map(currentGenre => {
            return <Genre genre={currentGenre} deleteGenre={this.deleteGenre} key={currentGenre._id}/>
        })
    }

    render() {
        return (
          <div>
              <h3>Gêneros</h3>
              <table className="table">
                  <thead className="thead-light">
                      <tr>
                          <th>Nome</th>
                          <th>Ações</th>
                      </tr>
                  </thead>
                  <tbody>
                      { this.genreList() }
                  </tbody>
              </table>
          </div>  
        );
    }
}