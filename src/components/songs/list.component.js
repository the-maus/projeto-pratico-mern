import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

const Song = props => (
    <tr>
        <td>{props.song.title}</td>
        <td>{props.song.artist}</td>
        <td>{props.song.album}</td>
        <td>{props.song.genre}</td>
        <td>{props.song.length}s</td>
        <td>
            <Link to={"/edit-song/"+props.song._id}>editar</Link> | <a href="#" onClick={() => props.deleteSong(props.song._id)}>remover</a>
        </td>
    </tr>
)

export default class SongsList extends Component {
    constructor(props) {
        super(props);

        this.deleteSong = this.deleteSong.bind(this);

        this.state = {songs:[]};
    }

    componentDidMount() {
        axios.get('http://localhost:5000/songs/')
            .then(res => {
                this.setState({songs: res.data});
            })
            .catch((error) => {
                console.log(error);
            })
    }

    deleteSong(id) {
        axios.delete('http://localhost:5000/songs/' + id)
            .then(res => console.log(res.data));

        this.setState({
            songs: this.state.songs.filter(el => el._id !== id)
        })
    }

    songList() {
        return this.state.songs.map(currentSong => {
            return <Song song={currentSong} deleteSong={this.deleteSong} key={currentSong._id}/>
        })
    }

    render() {
        return (
          <div>
              <h3>Músicas</h3>
              <table className="table">
                  <thead className="thead-light">
                      <tr>
                          <th>Título</th>
                          <th>Artista</th>
                          <th>Álbum</th>
                          <th>Gênero</th>
                          <th>Duração</th>
                          <th>Ações</th>
                      </tr>
                  </thead>
                  <tbody>
                      { this.songList() }
                  </tbody>
              </table>
          </div>  
        );
    }
}