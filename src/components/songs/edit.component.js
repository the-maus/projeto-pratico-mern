import React, { Component } from 'react';
import axios from 'axios';

export default class EditSong extends Component {
    constructor(props) {
        super(props);

        this.onChangeArtist = this.onChangeArtist.bind(this);
        this.onChangeAlbum = this.onChangeAlbum.bind(this);
        this.onChangeGenre = this.onChangeGenre.bind(this);
        this.onChangeTitle = this.onChangeTitle.bind(this);
        this.onChangeLength = this.onChangeLength.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            title: '',
            length: 0,
            artist: '',
            artists: [],
            album: '',
            albums: [],
            genre: '',
            genres: []
        }
    }

    componentDidMount() {
        axios.get('http://localhost:5000/songs/'+this.props.match.params.id)
            .then(res => {
                this.setState({
                    title: res.data.title,
                    artist: res.data.artist,
                    length: res.data.length,
                    album: res.data.album,
                    genre: res.data.genre
                })
            })
            .catch(function (error) {
                console.log(error);
            })

        axios.get('http://localhost:5000/artists/')
            .then(res => {
                if (res.data.length > 0) {
                    this.setState({
                        artists: res.data.map(artist => artist.name),
                    })
                }
            })

        axios.get('http://localhost:5000/albums/')
            .then(res => {
                if (res.data.length > 0) {
                    this.setState({
                        albums: res.data.map(album => album.title),
                    })
                }
            })

        axios.get('http://localhost:5000/genres/')
            .then(res => {
                if (res.data.length > 0) {
                    this.setState({
                        genres: res.data.map(genre => genre.name),
                    })
                }
            })
    }

    onChangeArtist(e) {
        this.setState({
            artist: e.target.value
        });
    }

    onChangeAlbum(e) {
        this.setState({
            album: e.target.value
        });
    }

    onChangeGenre(e) {
        this.setState({
            genre: e.target.value
        });
    }

    onChangeTitle(e) {
        this.setState({
            title: e.target.value
        });
    }

    onChangeLength(e) {
        this.setState({
            length: e.target.value
        });
    }

    onSubmit(e) {
        e.preventDefault();

        const song = {
            artist: this.state.artist,
            title: this.state.title,
            length: this.state.length,
            album: this.state.album,
            genre: this.state.genre,
        }

        console.log(song);

        axios.post('http://localhost:5000/songs/update/'+this.props.match.params.id, song)
            .then(res => console.log(res.data));

        window.location = '/songs';
    }

    render() {
        return (
            <div>
                <h3>Editar música</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Título: </label>
                        <input type="text" required className="form-control" value={this.state.title} onChange={this.onChangeTitle}/>
                    </div>
                    <div className="form-group">
                        <label>Artista: </label>
                        <select ref="artistInput" required className="form-control" value={this.state.artist} onChange={this.onChangeArtist}>
                            {
                                this.state.artists.map(function (artist) {
                                    return <option key={artist} value={artist}>{artist}</option>;
                                })
                            }
                        </select>
                    </div>
                    <div className="form-group">
                        <label>Álbum: </label>
                        <select ref="albumInput" required className="form-control" value={this.state.album} onChange={this.onChangeAlbum}>
                            {
                                this.state.albums.map(function (album) {
                                    return <option key={album} value={album}>{album}</option>;
                                })
                            }
                        </select>
                    </div>
                    <div className="form-group">
                        <label>Gênero: </label>
                        <select ref="genreInput" required className="form-control" value={this.state.genre} onChange={this.onChangeGenre}>
                            {
                                this.state.genres.map(function (genre) {
                                    return <option key={genre} value={genre}>{genre}</option>;
                                })
                            }
                        </select>
                    </div>
                    <div className="form-group">
                        <label>Duração (em segundos): </label>
                        <input type="number" required className="form-control" value={this.state.length} onChange={this.onChangeLength}/>
                    </div>
                    <div className="form-group">
                        <input type="submit" required className="btn btn-primary" value="Enviar"/>
                    </div>
                </form>
            </div>
        );
    }
}