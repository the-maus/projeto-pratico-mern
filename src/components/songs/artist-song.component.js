import React, { Component } from 'react';
import axios from 'axios';
import queryString from 'query-string';
import '../../App.css'

const Song = props => (
    <tr>
        <td>{props.song.title}</td>
        <td>{props.song.artist}</td>
        <td>{props.song.album}</td>
        <td>{props.song.genre}</td>
        <td>{props.song.length}s</td>
    </tr>
)

export default class ArtistSongsList extends Component {
    constructor(props) {
        super(props);

        this.onChangeArtist = this.onChangeArtist.bind(this);

        this.state = {
            songs: [],
            search: '',
            artist: '',
            artists: []
        };
    }

    componentDidMount() {
        const values = queryString.parse(this.props.location.search);
        let url = 'http://localhost:5000/songs';

        if (values) {
            if (values.search === null ||values.search === undefined || values.search == '') {
                delete values.search;
            } else {
                this.setState({search: values.search});
            }

            if (values.artist === null || values.artist === undefined || values.artist == '') {
                delete values.artist;
            } else {
                this.setState({artist: values.artist});
            }

            const searchParams = new URLSearchParams(values);
            url += '?' + searchParams;
            console.log(url);
        }

        axios.get(url)
            .then(res => {
                this.setState({ songs: res.data });
            })
            .catch((error) => {
                console.log(error);
            })

        axios.get('http://localhost:5000/artists/')
            .then(res => {
                if (res.data.length > 0) {
                    this.setState({
                        artists: res.data.map(artist => artist.name)
                    });
                }
            })
    }

    songList() {
        return this.state.songs.map(currentSong => {
            return <Song song={currentSong} key={currentSong._id} />
        })
    }

    onChangeArtist(e) {
        this.setState({
            artist: e.target.value
        });
    }

    print() {
        window.print();
    }

    render() {
        return (
            <div>
                <h3>Músicas</h3>
                <form action="/songs-by-artist">
                    <div className="form-group">
                        <label>Artista: </label>
                        <select ref="artistInput" className="form-control" name="artist" value={this.state.artist} onChange={this.onChangeArtist}>
                            <option></option>
                            {
                                this.state.artists.map(function (artist) {
                                    return <option key={artist} value={artist}>{artist}</option>;
                                })
                            }
                        </select>
                    </div>
                    <div className="form-group">
                        <label>Nome: </label>
                        <input type="text" className="form-control" name="search" defaultValue={this.state.search}/>
                    </div>
                    <div className="form-group no-print">
                        <input type="submit" required className="btn btn-primary" value="Filtrar" />&nbsp;
                        <button required className="btn btn-primary" onClick={this.print}>Imprimir</button>
                    </div>
                </form>
                <table className="table">
                    <thead className="thead-light">
                        <tr>
                            <th>Título</th>
                            <th>Artista</th>
                            <th>Álbum</th>
                            <th>Gênero</th>
                            <th>Duração</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.songList()}
                    </tbody>
                </table>
            </div>
        );
    }
}