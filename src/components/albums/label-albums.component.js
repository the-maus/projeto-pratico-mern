import React, { Component } from 'react';
import axios from 'axios';
import queryString from 'query-string';
import '../../App.css'

const Album = props => (
    <tr>
        <td>{props.album.title}</td>
        <td>{props.album.year}</td>
        <td>{props.album.label}</td>
    </tr>
)

export default class LabelAlbumsList extends Component {
    constructor(props) {
        super(props);

        this.onChangeLabel = this.onChangeLabel.bind(this);

        this.state = {
            albums: [],
            search: '',
            label: '',
            labels: []
        };
    }

    componentDidMount() {
        const values = queryString.parse(this.props.location.search);
        let url = 'http://localhost:5000/albums';

        if (values) {
            if (values.search === null ||values.search === undefined || values.search == '') {
                delete values.search;
            } else {
                this.setState({search: values.search});
            }

            if (values.label === null || values.label === undefined || values.label == '') {
                delete values.label;
            } else {
                this.setState({label: values.label});
            }

            const searchParams = new URLSearchParams(values);
            url += '?' + searchParams;
        }

        axios.get(url)
            .then(res => {
                this.setState({ albums: res.data });
            })
            .catch((error) => {
                console.log(error);
            })

        axios.get('http://localhost:5000/labels/')
            .then(res => {
                if (res.data.length > 0) {
                    this.setState({
                        labels: res.data.map(label => label.name)
                    });
                }
            })
    }

    albumList() {
        return this.state.albums.map(currentAlbum => {
            return <Album album={currentAlbum} key={currentAlbum._id}/>
        })
    }

    onChangeLabel(e) {
        this.setState({
            label: e.target.value
        });
    }

    print() {
        window.print();
    }

    render() {
        return (
            <div>
                <h3>Álbuns</h3>
                <form action="/albums-by-label">
                    <div className="form-group">
                        <label>Gravadora: </label>
                        <select ref="labelInput" className="form-control" name="label" value={this.state.label} onChange={this.onChangeLabel}>
                            <option></option>
                            {
                                this.state.labels.map(function (label) {
                                    return <option key={label} value={label}>{label}</option>;
                                })
                            }
                        </select>
                    </div>
                    <div className="form-group">
                        <label>Nome: </label>
                        <input type="text" className="form-control" name="search" defaultValue={this.state.search}/>
                    </div>
                    <div className="form-group no-print">
                        <input type="submit" required className="btn btn-primary" value="Filtrar" />&nbsp;
                        <button required className="btn btn-primary" onClick={this.print}>Imprimir</button>
                    </div>
                </form>
                <table className="table">
                  <thead className="thead-light">
                      <tr>
                          <th>Título</th>
                          <th>Ano</th>
                          <th>Gravadora</th>
                      </tr>
                  </thead>
                  <tbody>
                      { this.albumList() }
                  </tbody>
              </table>
            </div>
        );
    }
}