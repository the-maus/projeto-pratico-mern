import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

const Album = props => (
    <tr>
        <td>{props.album.title}</td>
        <td>{props.album.year}</td>
        <td>{props.album.label}</td>
        <td>
            <Link to={"/edit-album/"+props.album._id}>editar</Link> | <a href="#" onClick={() => props.deleteAlbum(props.album._id)}>remover</a>
        </td>
    </tr>
)

export default class AlbumsList extends Component {
    constructor(props) {
        super(props);

        this.deleteAlbum = this.deleteAlbum.bind(this);

        this.state = {albums:[]};
    }

    componentDidMount() {
        axios.get('http://localhost:5000/albums/')
            .then(res => {
                this.setState({albums: res.data});
            })
            .catch((error) => {
                console.log(error);
            })
    }

    deleteAlbum(id) {
        axios.delete('http://localhost:5000/albums/' + id)
            .then(res => console.log(res.data));

        this.setState({
            albums: this.state.albums.filter(el => el._id !== id)
        })
    }

    albumList() {
        return this.state.albums.map(currentAlbum => {
            return <Album album={currentAlbum} deleteAlbum={this.deleteAlbum} key={currentAlbum._id}/>
        })
    }

    render() {
        return (
          <div>
              <h3>Álbuns</h3>
              <table className="table">
                  <thead className="thead-light">
                      <tr>
                          <th>Título</th>
                          <th>Ano</th>
                          <th>Gravadora</th>
                          <th>Ações</th>
                      </tr>
                  </thead>
                  <tbody>
                      { this.albumList() }
                  </tbody>
              </table>
          </div>  
        );
    }
}