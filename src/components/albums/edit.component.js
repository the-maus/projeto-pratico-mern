import React, { Component } from 'react';
import axios from 'axios';

export default class EditAlbum extends Component {
    constructor(props) {
        super(props);

        this.onChangeTitle = this.onChangeTitle.bind(this);
        this.onChangeYear = this.onChangeYear.bind(this);
        this.onChangeLabel = this.onChangeLabel.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            title: '',
            year: 2020,
            label: '',
            labels: []
        }
    }

    componentDidMount() {
        axios.get('http://localhost:5000/albums/'+this.props.match.params.id)
            .then(res => {
                this.setState({
                    title: res.data.title,
                    year: res.data.year,
                    label: res.data.label
                })
            })
            .catch(function (error) {
                console.log(error);
            })

        axios.get('http://localhost:5000/labels/')
            .then(res => {
                if (res.data.length > 0) {
                    this.setState({
                        labels: res.data.map(label => label.name),
                    })
                }
            })
    }

    onChangeLabel(e) {
        this.setState({
            label: e.target.value
        });
    }

    onChangeTitle(e) {
        this.setState({
            title: e.target.value
        });
    }

    onChangeYear(e) {
        this.setState({
            year: e.target.value
        });
    }

    onSubmit(e) {
        e.preventDefault();

        const album = {
            title: this.state.title,
            year: this.state.year,
            label: this.state.label
        }

        console.log(album);

        axios.post('http://localhost:5000/albums/update/'+this.props.match.params.id, album)
            .then(res => console.log(res.data));

        window.location = '/albums';
    }

    render() {
        return (
            <div>
                <h3>Editar álbum</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Título: </label>
                        <input type="text" required className="form-control" value={this.state.title} onChange={this.onChangeTitle}/>
                    </div>
                    <div className="form-group">
                        <label>Ano: </label>
                        <input type="number" required className="form-control" value={this.state.year} onChange={this.onChangeYear}/>
                    </div>
                    <div className="form-group">
                        <label>Gravadora: </label>
                        <select ref="labelInput" required className="form-control" value={this.state.label} onChange={this.onChangeLabel}>
                            {
                                this.state.labels.map(function (label) {
                                    return <option key={label} value={label}>{label}</option>;
                                })
                            }
                        </select>
                    </div>
                    <div className="form-group">
                        <input type="submit" required className="btn btn-primary" value="Enviar"/>
                    </div>
                </form>
            </div>
        );
    }
}