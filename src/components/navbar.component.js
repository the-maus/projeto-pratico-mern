import React, { Component, Dropdown } from 'react';
import { Link } from 'react-router-dom';

export default class Navbar extends Component {
    
    render () {
        return (
            <nav className="navbar navbar-dark bg-dark navbar-expand-lg">
                <Link to="/songs" className="navbar-brand">Controle de Músicas</Link>
                <div className="navbar">
                    <ul className="navbar-nav mr-auto">
                        <li className="navbar-item">
                            <div className="dropdown">
                                <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownSongs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Músicas
                                </button>
                                <div className="dropdown-menu" aria-labelledby="dropdownSongs">
                                    <Link to="/songs" className="dropdown-item">Listar</Link>
                                    <Link to="/create-song" className="dropdown-item">Cadastrar</Link>
                                </div>
                            </div>
                        </li>&nbsp;
                        <li className="navbar-item">
                            <div className="dropdown">
                                <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownArtists" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Artistas
                                </button>
                                <div className="dropdown-menu" aria-labelledby="dropdownArtists">
                                    <Link to="/artists" className="dropdown-item">Listar</Link>
                                    <Link to="/create-artist" className="dropdown-item">Cadastrar</Link>
                                </div>
                            </div>
                        </li>&nbsp;
                        <li className="navbar-item">
                            <div className="dropdown">
                                <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownGenres" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Gênero
                                </button>
                                <div className="dropdown-menu" aria-labelledby="dropdownGenres">
                                    <Link to="/genres" className="dropdown-item">Listar</Link>
                                    <Link to="/create-genre" className="dropdown-item">Cadastrar</Link>
                                </div>
                            </div>
                        </li>&nbsp;
                        <li className="navbar-item">
                            <div className="dropdown">
                                <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownLabels" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Gravadora
                                </button>
                                <div className="dropdown-menu" aria-labelledby="dropdownLabels">
                                    <Link to="/labels" className="dropdown-item">Listar</Link>
                                    <Link to="/create-label" className="dropdown-item">Cadastrar</Link>
                                </div>
                            </div>
                        </li>&nbsp;
                        <li className="navbar-item">
                            <div className="dropdown">
                                <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownAlbums" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Álbum
                                </button>
                                <div className="dropdown-menu" aria-labelledby="dropdownAlbums">
                                    <Link to="/albums" className="dropdown-item">Listar</Link>
                                    <Link to="/create-album" className="dropdown-item">Cadastrar</Link>
                                </div>
                            </div>
                        </li>
                        &nbsp;
                        <li className="navbar-item">
                            <div className="dropdown">
                                <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownReports" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Relatórios
                                </button>
                                <div className="dropdown-menu" aria-labelledby="dropdownReports">
                                    <Link to="/songs-by-artist" className="dropdown-item">Músicas por artista</Link>
                                    <Link to="/songs-by-genre" className="dropdown-item">Músicas por gênero</Link>
                                    <Link to="/albums-by-label" className="dropdown-item">Álbuns por gravadora</Link>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
}