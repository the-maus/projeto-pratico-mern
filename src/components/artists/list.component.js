import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

const Artist = props => (
    <tr>
        <td>{props.artist.name}</td>
        <td>
            <Link to={"/edit-artist/"+props.artist._id}>editar</Link> | <a href="#" onClick={() => props.deleteArtist(props.artist._id)}>remover</a>
        </td>
    </tr>
)

export default class ArtistsList extends Component {
    constructor(props) {
        super(props);

        this.deleteArtist = this.deleteArtist.bind(this);

        this.state = {artists:[]};
    }

    componentDidMount() {
        axios.get('http://localhost:5000/artists/')
            .then(res => {
                this.setState({artists: res.data});
            })
            .catch((error) => {
                console.log(error);
            })
    }

    deleteArtist(id) {
        axios.delete('http://localhost:5000/artists/' + id)
            .then(res => console.log(res.data));

        this.setState({
            artists: this.state.artists.filter(el => el._id !== id)
        })
    }

    artistList() {
        return this.state.artists.map(currentArtist => {
            return <Artist artist={currentArtist} deleteArtist={this.deleteArtist} key={currentArtist._id}/>
        })
    }

    render() {
        return (
          <div>
              <h3>Artistas</h3>
              <table className="table">
                  <thead className="thead-light">
                      <tr>
                          <th>Nome</th>
                          <th>Ações</th>
                      </tr>
                  </thead>
                  <tbody>
                      { this.artistList() }
                  </tbody>
              </table>
          </div>  
        );
    }
}