const router = require('express').Router();
let Artist = require('../models/artist.model');

router.route('/').get((req, res) => {
    Artist.find()
        .then(artists => res.json(artists))
        .catch(err => res.status(400).json('Erro: ' + err));
});

router.route('/add').post((req, res) => {
    const name = req.body.name;
    const newArtist = new Artist({name});

    newArtist.save()
        .then(() => res.json('Artista adicionado.'))
        .catch(err => res.status(400).json('Erro: ' + err));
});

router.route('/:id').get((req, res) => {
    Artist.findById(req.params.id)
        .then(song => res.json(song))
        .catch(err => res.status(400).json('Erro: '+ err));
});

router.route('/:id').delete((req, res) => {
    Artist.findByIdAndDelete(req.params.id)
        .then(() => res.json('Artista removido.'))
        .catch(err => res.status(400).json('Erro: '+ err));
});

router.route('/update/:id').post((req, res) => {
    Artist.findById(req.params.id)
        .then(artist => {
            artist.name = req.body.name;

            artist.save()
                .then(() => res.json('Artista atualizado.'))
                .catch(err => res.status(400).json('Erro: ' + err));
        })
        .catch(err => res.status(400).json('Erro: '+ err));
});

module.exports = router;