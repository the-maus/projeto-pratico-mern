const router = require('express').Router();
let Genre = require('../models/genre.model');

router.route('/').get((req, res) => {
    Genre.find()
        .then(genres => res.json(genres))
        .catch(err => res.status(400).json('Erro: ' + err));
});

router.route('/add').post((req, res) => {
    const name = req.body.name;
    const newGenre = new Genre({name});

    newGenre.save()
        .then(() => res.json('Gênero adicionado.'))
        .catch(err => res.status(400).json('Erro: ' + err));
});

router.route('/:id').get((req, res) => {
    Genre.findById(req.params.id)
        .then(genre => res.json(genre))
        .catch(err => res.status(400).json('Erro: '+ err));
});

router.route('/:id').delete((req, res) => {
    Genre.findByIdAndDelete(req.params.id)
        .then(() => res.json('Gênero removido.'))
        .catch(err => res.status(400).json('Erro: '+ err));
});

router.route('/update/:id').post((req, res) => {
    Genre.findById(req.params.id)
        .then(genre => {
            genre.name = req.body.name;

            genre.save()
                .then(() => res.json('Gênero atualizado.'))
                .catch(err => res.status(400).json('Erro: ' + err));
        })
        .catch(err => res.status(400).json('Erro: '+ err));
});

module.exports = router;