const router = require('express').Router();
let Album = require('../models/album.model');

router.route('/').get((req, res) => {
    let {label, search} = req.query;
    let query = {};
    if (label != null) query.label = label;
    if (search != null) query.title = { $regex: '.*' + search + '.*', $options: "i" };

    Album.find(query)
        .then(albums => res.json(albums))
        .catch(err => res.status(400).json('Erro: ' + err));
});

router.route('/add').post((req, res) => {
    const title = req.body.title;
    const year = Number(req.body.year);
    const label = req.body.label;

    const newAlbum = new Album({
        title,
        year,
        label
    });

    newAlbum.save()
        .then(() => res.json('Álbum adicionado.'))
        .catch(err => res.status(400).json('Erro: ' + err));
});

router.route('/:id').get((req, res) => {
    Album.findById(req.params.id)
        .then(album => res.json(album))
        .catch(err => res.status(400).json('Erro: '+ err));
});

router.route('/:id').delete((req, res) => {
    Album.findByIdAndDelete(req.params.id)
        .then(() => res.json('Álbum removido.'))
        .catch(err => res.status(400).json('Erro: '+ err));
});

router.route('/update/:id').post((req, res) => {
    Album.findById(req.params.id)
        .then(album => {
            album.title = req.body.title;
            album.year = Number(req.body.year);
            album.label = req.body.label;

            album.save()
                .then(() => res.json('Álbum atualizado.'))
                .catch(err => res.status(400).json('Erro: ' + err));
        })
        .catch(err => res.status(400).json('Erro: '+ err));
});

module.exports = router;