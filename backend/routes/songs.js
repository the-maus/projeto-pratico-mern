const router = require('express').Router();
let Song = require('../models/song.model');

router.route('/').get((req, res) => {
    let {artist, genre, search} = req.query;
    let query = {};
    if (artist != null) query.artist = artist;
    if (genre != null) query.genre = genre;
    if (search != null) query.title = { $regex: '.*' + search + '.*', $options: "i" };
    
    Song.find(query)
        .then(songs => res.json(songs))
        .catch(err => res.status(400).json('Erro: ' + err));
});

router.route('/add').post((req, res) => {
    const title = req.body.title;
    const length = Number(req.body.length);
    const artist = req.body.artist;
    const album = req.body.album;
    const genre = req.body.genre;

    const newSong = new Song({
        title,
        length,
        artist,
        album,
        genre
    });

    newSong.save()
        .then(() => res.json('Música adicionada.'))
        .catch(err => res.status(400).json('Erro: ' + err));
});

router.route('/:id').get((req, res) => {
    Song.findById(req.params.id)
        .then(song => res.json(song))
        .catch(err => res.status(400).json('Erro: '+ err));
});

router.route('/:id').delete((req, res) => {
    Song.findByIdAndDelete(req.params.id)
        .then(() => res.json('Música removida.'))
        .catch(err => res.status(400).json('Erro: '+ err));
});

router.route('/update/:id').post((req, res) => {
    Song.findById(req.params.id)
        .then(song => {
            song.title = req.body.title;
            song.length = Number(req.body.length);
            song.artist = req.body.artist;
            song.album = req.body.album;
            song.genre = req.body.genre;

            song.save()
                .then(() => res.json('Música atualizada.'))
                .catch(err => res.status(400).json('Erro: ' + err));
        })
        .catch(err => res.status(400).json('Erro: '+ err));
});

module.exports = router;