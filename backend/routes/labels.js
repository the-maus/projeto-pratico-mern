const router = require('express').Router();
let RecordLabel = require('../models/record_label.model');

router.route('/').get((req, res) => {
    RecordLabel.find()
        .then(labels => res.json(labels))
        .catch(err => res.status(400).json('Erro: ' + err));
});

router.route('/add').post((req, res) => {
    const name = req.body.name;
    const newLabel = new RecordLabel({name});

    newLabel.save()
        .then(() => res.json('Gravadora adicionada.'))
        .catch(err => res.status(400).json('Erro: ' + err));
});

router.route('/:id').get((req, res) => {
    RecordLabel.findById(req.params.id)
        .then(label => res.json(label))
        .catch(err => res.status(400).json('Erro: '+ err));
});

router.route('/:id').delete((req, res) => {
    RecordLabel.findByIdAndDelete(req.params.id)
        .then(() => res.json('Gravadora removida.'))
        .catch(err => res.status(400).json('Erro: '+ err));
});

router.route('/update/:id').post((req, res) => {
    RecordLabel.findById(req.params.id)
        .then(label => {
            label.name = req.body.name;

            label.save()
                .then(() => res.json('Gravadora atualizada.'))
                .catch(err => res.status(400).json('Erro: ' + err));
        })
        .catch(err => res.status(400).json('Erro: '+ err));
});

module.exports = router;