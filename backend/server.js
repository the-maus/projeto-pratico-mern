const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

require('dotenv').config();

const app = express();
const port = process.env.PORT || 5000;

app.use(cors());
app.use(express.json());

const uri = process.env.MONGODB_URI;
mongoose.connect(uri, {useNewUrlParser: true, useCreateIndex: true});
const connection = mongoose.connection;
connection.once('open', () => {
    console.log("Conexão com o MongoDB estabelecida com sucesso.")
});

const songsRouter = require('./routes/songs');
const artistsRouter = require('./routes/artists');
const genresRouter = require('./routes/genres');
const labelsRouter = require('./routes/labels');
const albumsRouter = require('./routes/albums');

app.use('/songs', songsRouter);
app.use('/artists', artistsRouter);
app.use('/genres', genresRouter);
app.use('/labels', labelsRouter);
app.use('/albums', albumsRouter);

app.listen(port, () => {
    console.log(`Servidor rodando na porta: ${port}`);
});