const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const recordLabelSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true,
        trim: true,
        minlength: 3  
      }
});

const RecordLabel = mongoose.model('RecordLabel', recordLabelSchema);

module.exports = RecordLabel;