const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const albumSchema = new Schema({
    title: {type: String, required: true},
    year: {type: Number, required: true},
    label: {type: String, required: true},
});

const Album = mongoose.model('Album', albumSchema);

module.exports = Album;